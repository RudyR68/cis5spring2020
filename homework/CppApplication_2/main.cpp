/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 14, 2020, 8:18 AM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // step one: opening a file
    ofstream fout;
    
    //dot operator
    fout.open("test.txt");
    
    // manipulate the file
    // can use variable (fout) like cout
    fout << "Hello world" << endl;
    
    //close file
    fout.close();
    
    // loop examples
    
    //"for" loop syntax is for known number of loops
    for(int i = 0; i <10; i++)
    {
            cout << i << " ";
    }
    cout << endl << "while loop:" << endl; 
    //"while" loop
    int i = 0; //initializer
    while (i < 10)
    {
        //incrementor 
        i++;
        cout << i + 1 << " ";
    }
    cout << " would you like to run the program? (yes/no)" << endl;
    string input;
    cin >> input;
    //counter 
    int count = 0; 
    
   // user checking
    while (tolower(input[0]) == 'y') 
    {
        count++;
        cout << "you have ran the program " << count << "time(s)!" << endl;
        //re-prompt
        cout << "would you like to run the program again?";
        cin >> input;    
    }
    // do while
    do 
    {
        count++;
        cout << "count: " << count <<endl;
        //even count
        if (count % 2 == 0)
        {
            cout << "even count" << endl;
        }
        else
        {
            cout << "odd count";
        }
        cout << "would you like to run again?" << endl;
        cin >> input;
    } while (tolower(input[0]) == 'y');
   
    return 0;
}

